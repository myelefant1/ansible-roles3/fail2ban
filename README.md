## Ansible configuration of fail2ban

This role configures fail2ban

Tested on debian 10, 11 & 12.

## Role parameters

| name                        | value    | optionnal | default value   | description                            |
| ----------------------------|----------|-----------|-----------------|----------------------------------------|
| fail2ban_whitelist_template | string   | yes       | no              | IPs whitelist                          |
| fail2ban_jail_local_template| string   | yes       | no              | custom configuration file              |

## Using this role

### ansible galaxy

Add the following in your *requirements.yml*.

```yaml
---
  src: https://gitlab.com/myelefant1/ansible-roles3/fail2ban.git
  scm: git
```

### Sample playbook

```yaml
- hosts: all
  roles:
    - role: fail2ban
      fail2ban_whitelist_template: templates/my_whitelist.txt
      fail2ban_jail_local_template: templates/jail.local
```

## Tests

[tests/tests_fail2ban](tests/tests_fail2ban)
